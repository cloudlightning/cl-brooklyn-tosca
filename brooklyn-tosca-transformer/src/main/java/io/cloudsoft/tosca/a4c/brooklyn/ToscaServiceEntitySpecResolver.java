package io.cloudsoft.tosca.a4c.brooklyn;

import io.cloudsoft.tosca.a4c.brooklyn.plan.ToscaTypePlanTransformer;
import org.apache.brooklyn.api.entity.EntitySpec;
import org.apache.brooklyn.api.mgmt.classloading.BrooklynClassLoadingContext;
import org.apache.brooklyn.core.resolve.entity.AbstractEntitySpecResolver;

import java.util.Set;

public class ToscaServiceEntitySpecResolver extends AbstractEntitySpecResolver {

    private static final String RESOLVER_NAME = "alien4cloud_deployment_topology_service";

    private ToscaTypePlanTransformer toscaTypePlanTransformer;

    public ToscaServiceEntitySpecResolver() {
        super(RESOLVER_NAME);
    }

    @Override
    public EntitySpec<?> resolve(String type, BrooklynClassLoadingContext loader, Set<String> encounteredTypes) {
        if (null == toscaTypePlanTransformer) {
            toscaTypePlanTransformer = new ToscaTypePlanTransformer();
        }
        toscaTypePlanTransformer.setManagementContext(mgmt);

        return toscaTypePlanTransformer.createApplicationSpecFromTopologyAndServiceId(getLocalType(type));
    }

    // Note this is used to inject the type plan transformer bean in the OSGI blueprint.xml context.
    public void setToscaTypePlanTransformer(ToscaTypePlanTransformer toscaTypePlanTransformer) {
        this.toscaTypePlanTransformer = toscaTypePlanTransformer;
    }
}
