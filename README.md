brooklyn-tosca
===

## Overview

This package provides support for [Apache Brooklyn](http://brooklyn.io)
to understand [OASIS TOSCA](https://www.oasis-open.org/committees/tosca/) plans,
using [Alien4Cloud](http://alien4cloud.github.io).

It can be run as a standalone file, launching Brooklyn, or the JAR dropped in to your own Brooklyn.


## Build Notes

You'll need the right version of Alien4Cloud installed to your maven repository; This can be found [here](https://bitbucket.org/cloudlightning/gateway-dependency-a4c)

Then simply:

    mvn clean install assembly:single


## Running

See the README.md file in the resulting archive (in `target`) for runtime instructions.
You may also find that file [here](brooklyn-tosca-dist/src/main/assembly/files/README.md).
