package alien4cloud.cloudlightning.model.sde;

/**
 * Created by adrian on 08.03.2017.
 */
public enum RequestStatus {
    NOT_STARTED,
    PROCESSING,
    SUCCESS,
    FAIL
}
