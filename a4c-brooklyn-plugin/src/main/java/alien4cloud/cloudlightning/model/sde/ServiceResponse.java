package alien4cloud.cloudlightning.model.sde;

/**
 * Created by adrian on 02.07.2017.
 */
public class ServiceResponse {
    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
