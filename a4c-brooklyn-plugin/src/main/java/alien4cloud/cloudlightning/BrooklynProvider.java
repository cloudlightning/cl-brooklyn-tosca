package alien4cloud.cloudlightning;

import alien4cloud.application.ApplicationService;
import alien4cloud.cloudlightning.model.CLGroup;
import alien4cloud.cloudlightning.model.sde.ServiceResource;
import alien4cloud.dao.IGenericSearchDAO;
import alien4cloud.exception.NotFoundException;
import alien4cloud.marathon.service.events.EventService;
import alien4cloud.model.application.Application;
import alien4cloud.model.common.Tag;
import alien4cloud.orchestrators.locations.services.LocationService;
import alien4cloud.paas.IConfigurablePaaSProvider;
import alien4cloud.paas.IPaaSCallback;
import alien4cloud.paas.exception.MaintenanceModeException;
import alien4cloud.paas.exception.OperationExecutionException;
import alien4cloud.paas.exception.PluginConfigurationException;
import alien4cloud.paas.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Functions;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import io.cloudsoft.tosca.metadata.RequiresBrooklynApi;
import io.cloudsoft.tosca.metadata.ToscaTypeProvider;
import lombok.SneakyThrows;

import mesosphere.marathon.client.MarathonException;
import mesosphere.marathon.client.model.v2.*;
import org.alien4cloud.tosca.model.definitions.FunctionPropertyValue;
import org.alien4cloud.tosca.model.definitions.IValue;
import org.alien4cloud.tosca.model.definitions.Operation;
import org.apache.brooklyn.core.entity.lifecycle.Lifecycle;
import org.apache.brooklyn.rest.client.BrooklynApi;
import org.apache.brooklyn.rest.domain.ApplicationSummary;
import org.apache.brooklyn.rest.domain.EntitySummary;
import org.apache.brooklyn.rest.domain.Status;
import org.apache.brooklyn.rest.domain.TaskSummary;
import org.apache.brooklyn.util.text.Strings;
import org.elasticsearch.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.Map.Entry;

import static com.google.common.collect.Maps.newHashMap;

public abstract class BrooklynProvider implements IConfigurablePaaSProvider<Configuration> {
    private static final Logger log = LoggerFactory.getLogger(BrooklynProvider.class);

    private Configuration configuration;

    // TODO mock cache while we flesh out the impl
    protected Map<String,PaaSTopologyDeploymentContext> knownDeployments = Maps.newConcurrentMap();
    protected Map<String, Optional<DeploymentStatus>> deploymentStatuses = Maps.newConcurrentMap();

    // TODO: Sanity check correct InstanceStatuses are being used
    private static final Map<Lifecycle, InstanceStatus> LIFECYCLE_TO_INSTANCE_STATUS = ImmutableMap.<Lifecycle, InstanceStatus>builder()
            .put(Lifecycle.CREATED, InstanceStatus.PROCESSING)
            .put(Lifecycle.DESTROYED, InstanceStatus.MAINTENANCE)
            .put(Lifecycle.ON_FIRE, InstanceStatus.FAILURE)
            .put(Lifecycle.RUNNING, InstanceStatus.SUCCESS)
            .put(Lifecycle.STARTING, InstanceStatus.PROCESSING)
            .put(Lifecycle.STOPPED, InstanceStatus.MAINTENANCE)
            .put(Lifecycle.STOPPING, InstanceStatus.MAINTENANCE)
            .build();

    private static final Map<Status, DeploymentStatus> SERVICE_STATE_TO_DEPLOYMENT_STATUS = ImmutableMap.<Status, DeploymentStatus>builder()
            .put(Status.ACCEPTED, DeploymentStatus.DEPLOYMENT_IN_PROGRESS)
            .put(Status.STARTING, DeploymentStatus.DEPLOYMENT_IN_PROGRESS)
            .put(Status.RUNNING, DeploymentStatus.DEPLOYED)
            .put(Status.STOPPING, DeploymentStatus.UNDEPLOYMENT_IN_PROGRESS)
            .put(Status.STOPPED, DeploymentStatus.UNDEPLOYMENT_IN_PROGRESS)
            .put(Status.DESTROYED, DeploymentStatus.UNDEPLOYED)
            .put(Status.ERROR, DeploymentStatus.FAILURE)
            .put(Status.UNKNOWN, DeploymentStatus.UNKNOWN)
            .build();


    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private BrooklynCatalogMapper catalogMapper;

    @Autowired
    @Qualifier("alien-es-dao")
    private IGenericSearchDAO alienDAO;

    @Autowired
    private BeanFactory beanFactory;

    @Autowired
    private SDEClient sdeClient;

    @Autowired
    private CloudLightningMappingService cloudLightningMappingService;

    ThreadLocal<ClassLoader> oldContextClassLoader = new ThreadLocal<ClassLoader>();
    private Map<String, CLGroup> cloudLightningDeployments = Maps.newConcurrentMap();


    private void useLocalContextClassLoader() {
        if (oldContextClassLoader.get()==null) {
            oldContextClassLoader.set( Thread.currentThread().getContextClassLoader() );
        }
        Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
    }
    private void revertContextClassLoader() {
        if (oldContextClassLoader.get()==null) {
            log.warn("No local context class loader to revert");
        }
        Thread.currentThread().setContextClassLoader(oldContextClassLoader.get());
        oldContextClassLoader.remove();
    }

    @Override
    public void init(Map<String, PaaSTopologyDeploymentContext> activeDeployments) {
        useLocalContextClassLoader();
        try {
            log.info("INIT: " + activeDeployments);

//            catalogMapper.addBaseTypes();

            List<ToscaTypeProvider> metadataProviders = new LinkedList<>();
            for (String providerClass : configuration.getProviders()) {
                try {
                    Object provider = beanFactory.getBean(Class.forName(providerClass));
                    if(provider instanceof RequiresBrooklynApi) {
                        ((RequiresBrooklynApi) provider).setBrooklynApi(getNewBrooklynApi());
                    }
                    // Alien UI has higher priority items at the end of the list.
                    // Reverse the order here.
                    metadataProviders.add(0, ToscaTypeProvider.class.cast(provider));
                } catch (ClassNotFoundException e) {
                    log.warn("Could not load metadata provider " + providerClass, e);
                }
            }

//            catalogMapper.mapBrooklynEntities(getNewBrooklynApi(), new ToscaMetadataProvider(metadataProviders));

        } finally {
            revertContextClassLoader();
        }
    }

    @Override
    @SneakyThrows
    public void deploy(PaaSTopologyDeploymentContext deploymentContext, IPaaSCallback<?> callback) {
        log.info("DEPLOY "+deploymentContext+" / "+callback);
        String topologyId = deploymentContext.getDeploymentTopology().getId();
        String topologyVer = deploymentContext.getDeploymentTopology().getVersionId();
        String appId = topologyVer.split(":")[0];
        String appVer = topologyVer.split(":")[1];

        Map<String, ServiceResource> servicesLocation = sdeClient.getServicesLocation(appId, appVer);
        // Here check dependencies and cache service to orhcestrator clients
        CLGroup group = cloudLightningMappingService.buildGroupDefinition(deploymentContext, servicesLocation);


//        String locationIds[] = deploymentContext.getDeployment().getLocationIds();

//        if (locationIds.length > 0) {
//            campYaml.put("location", locationService.getOrFail(locationIds[0]).getName());
//        }
        knownDeployments.put(deploymentContext.getDeploymentId(), deploymentContext);

        startAppsAfterDependencies(group.getMarathonServices().get(0), deploymentContext, group);
//        deployBrooklyn(deploymentContext, callback, campYaml);

        if (callback!=null) callback.onSuccess(null);
    }



    private void startAppsAfterDependencies(Group group, PaaSTopologyDeploymentContext paaSTopologyDeploymentContext, CLGroup clGroup) {
        Map<String, Object> deployedApps = new HashMap<>();
        List<Object> apps = new ArrayList<>(group.getApps());
        for(Object t : clGroup.getBrooklynServices().values()){
            apps.add(t);
        }
        for(Object o : apps){
            if(o instanceof App) {
                App app = (App) o;
                app.setId("/" + group.getId() + "/" + app.getId());
            }
        }
        group.setApps(new ArrayList<>());
        Result groupResult = null;
//        try {
//            App app = (App) apps.stream().filter(t -> t instanceof App).findFirst().orElse(null);
//            String[] appIdTokens = apps.get(0).getId().split("/");
//            String appId = appIdTokens[appIdTokens.length - 1];
//            CLMarathon clMarathon = clGroup.getMarathonClients().get(appId);
//            groupResult = clMarathon.createGroup(group);
//            // Store the deployment ID to handle event mapping
//            if(groupResult != null){
//
//                clGroup.getMarathonEvents().get(appId).registerDeployment(groupResult.getDeploymentId(), paaSTopologyDeploymentContext.getDeploymentId(), DeploymentStatus.DEPLOYMENT_IN_PROGRESS);
//            }
//        } catch (MarathonException e) {
//            log.error("Failure while deploying - Got error code [" + e.getStatus() + "] with message: " + e.getMessage());
//        }
        List<App> toStart = new ArrayList<>();
        paaSTopologyDeploymentContext.getDeployment().setOrchestratorDeploymentId(paaSTopologyDeploymentContext.getDeploymentId());
        alienDAO.save(paaSTopologyDeploymentContext.getDeployment());
        // (the result is a 204 creating, whose entity is a TaskSummary
        // with an entityId of the entity which is created and id of the task)
        deploymentStatuses.put(paaSTopologyDeploymentContext.getDeploymentId(), Optional.<DeploymentStatus>absent());
        while(!apps.isEmpty()){
            Object o = apps.remove(0);
            if(o instanceof App) {
                group = deployMarathonApp(group, paaSTopologyDeploymentContext, clGroup, deployedApps, apps, (App) o);

            }else{
                deployBrooklynApp(group, paaSTopologyDeploymentContext, clGroup, deployedApps, apps, (PaaSNodeTemplate) o);
            }
            if(!clGroup.getDeployedApps().equals(deployedApps)){
                clGroup.setDeployedApps(deployedApps);
                cloudLightningDeployments.put(paaSTopologyDeploymentContext.getDeploymentId(), clGroup);
                deploymentStatuses.put(paaSTopologyDeploymentContext.getDeploymentId(), Optional.of(DeploymentStatus.DEPLOYED));
            }

        }
    }

    private Group deployMarathonApp(Group group, PaaSTopologyDeploymentContext deploymentContext, CLGroup clGroup, Map<String, Object> deployedApps, List<Object> apps, App app) {

        String[] appIdTokens = app.getId().split("/");
        String appId = appIdTokens[appIdTokens.length - 1];
        if (app.getDependencies() == null) {
            App result = clGroup.getMarathonClients().get(appId).createApp(app);
            clGroup.getMarathonEvents().get(appId).registerDeployment(result.getDeployments().get(0).getId(), deploymentContext.getDeploymentId(), DeploymentStatus.DEPLOYMENT_IN_PROGRESS);
            deployedApps.put(app.getId().toLowerCase(), result);

        } else {
            boolean depMet = true;
            for (String dep : clGroup.getDependencies().get(app.getLabels().get("name"))) {
                Object depApp = deployedApps.get(dep);
                if(depApp == null){
                    //no cloudlightning service deployed
                    //try to see if a marathon app is deployed
                    String dep2 = "/" + group.getId() + "/" + dep.toLowerCase();
                    depApp = deployedApps.get(dep2);
                }
                if (depApp == null) {
                    //dependency is not currently deployed; add app back to queue
                    apps.add(app);
                    depMet = false;
                    break;
                } else {
                    if (depApp instanceof App) {
                        App mApp = (App) depApp;
                        List<Deployment> deployments = clGroup.getMarathonClients().get(dep.toLowerCase()).getDeployments();
                        while (deployMentsContainDeps(deployments, app, group)) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            deployments = clGroup.getMarathonClients().get(dep.toLowerCase()).getDeployments();
                        }
                        final List<Task> tasks = clGroup.getMarathonClients().get(dep.toLowerCase()).getAppTasks(mApp.getId()).getTasks();
                        boolean isUp = isServiceUp(tasks);

                        if(!isUp){
                            depMet = false;
                            apps.add(app);
                            break;
                        }

                        app = replaceDependentEndpoints(app, mApp, deploymentContext, clGroup);
                    }else{
                        //TODO Check for a deployed cloudlightning app an get endpoints
                    }

                }
            }
            if (depMet) {

//                group = clGroup.getMarathonClients().get(app.getId().split("/")[2]).getGroup(group.getId());
//                group.getApps().add(app);
//                group.setVersion(null);
                App app1 = clGroup.getMarathonClients().get(app.getId().split("/")[2]).createApp(app);
                clGroup.getMarathonEvents().get(appId).registerDeployment(app1.getDeployments().get(0).getId(), deploymentContext.getDeploymentId(), DeploymentStatus.DEPLOYMENT_IN_PROGRESS);
                deployedApps.put(app.getId().toLowerCase(), app1);
            }
        }
        return group;
    }

    private boolean deployMentsContainDeps(List<Deployment> deployments, App app, Group group) {
        for(Deployment d : deployments){
            for (String a : app.getDependencies()){
                String dep = "/" + group.getId() + "/" + a  ;
                if(d.getAffectedApps().contains(dep))
                    return true;
            }
        }
        return false;
    }

    private App replaceDependentEndpoints(App app, App appDependency, PaaSTopologyDeploymentContext paaSTopologyDeploymentContext, CLGroup clGroup) {
        java.util.Optional<PaaSNodeTemplate> app1Node = paaSTopologyDeploymentContext.getPaaSTopology().getNonNatives().stream()
                .filter(p -> p.getId().toLowerCase().equals(app.getId().split("/")[2]))
                .findFirst();
        java.util.Optional<PaaSNodeTemplate> depNode = paaSTopologyDeploymentContext.getPaaSTopology().getNonNatives().stream()
                .filter(p -> p.getId().toLowerCase().equals(appDependency.getId().split("/")[2]))
                .findFirst();
        if(!(app1Node.isPresent() && depNode.isPresent()))
            return app;

        final Operation createOperation = app1Node.get().getInterfaces().get("tosca.interfaces.node.lifecycle.Standard").getOperations().get("create");
        if (createOperation.getInputParameters() != null) {
            createOperation.getInputParameters().forEach((key, val) -> {

                // Inputs can either be a ScalarValue or a pointer to a capability targeted by one of the node's requirements
                final List<Task> tasks = clGroup.getMarathonClients().get(appDependency.getId().split("/")[2]).getAppTasks(appDependency.getId()).getTasks();
                Task task = tasks.get(0);
                String value = cloudLightningMappingService.retrieveValue(app1Node.get(), paaSTopologyDeploymentContext.getPaaSTopology(), val, task);
                cloudLightningMappingService.addInputParameter(app, key, value);
            });
        }

        return app;
    }

    private Map<String, String> addDependentPropsFromMarathon(PaaSNodeTemplate brNode, App appDependency,
                                                              PaaSTopologyDeploymentContext deploymentContext, Task task) {
//        java.util.Optional<PaaSNodeTemplate> app1Node = deploymentContext.getPaaSTopology().getAllNodes().values().stream()
//                .filter(p -> p.getId().toLowerCase().equals(brNode.getId()))
//                .findFirst();
        PaaSNodeTemplate app1Node = deploymentContext.getPaaSTopology().getAllNodes().get(brNode.getId());
        java.util.Optional<PaaSNodeTemplate> depNode = deploymentContext.getPaaSTopology().getNonNatives().stream()
                .filter(p -> p.getId().toLowerCase().equals(appDependency.getId().split("/")[2]))
                .findFirst();
        if(!(app1Node != null && depNode.isPresent()))
            return null;

        final Operation createOperation = app1Node.getInterfaces().get("tosca.interfaces.node.lifecycle.Standard").getOperations().get("create");
        if (createOperation.getInputParameters() != null) {
            Map<String, String> property = new HashMap<>();

            createOperation.getInputParameters().forEach((key, val) -> {

                // Inputs can either be a ScalarValue or a pointer to a capability targeted by one of the node's requirements
//                final List<Task> tasks = clGroup.getMarathonClients().get(brNode.getId()).getAppTasks(appDependency.getId()).getTasks();
//                Task task = tasks.get(0);
                if (val instanceof FunctionPropertyValue && "get_property".equals(((FunctionPropertyValue) val).getFunction())) {
                    if ("REQ_TARGET".equals(((FunctionPropertyValue) val).getTemplateName())) {
                        String requirementName = ((FunctionPropertyValue) val).getCapabilityOrRequirementName();
                        String propertyName = ((FunctionPropertyValue) val).getElementNameToFetch();
                        String value = cloudLightningMappingService.retrieveValue(app1Node, deploymentContext.getPaaSTopology(), val, task);
//                cloudLightningMappingService.addInputParameter(brNode, key, value);

                        property.put(requirementName + "_" + propertyName, value);

                    }
                }

            });
            return property;
        }

        return null;
    }
    private Object deployBrooklynApp(Group group, PaaSTopologyDeploymentContext deploymentContext, CLGroup clGroup, Map<String, Object> deployedApps, List<Object> apps, PaaSNodeTemplate brooklynNode) {
        Map<String, Object> campYaml = createBasicBrooklynService(deploymentContext, clGroup, brooklynNode);

        if (clGroup.getDependencies().get(brooklynNode.getId()) == null) {
            campYaml.put("cloudlightning.config", ImmutableMap.of("tosca.deployment.id", deploymentContext.getDeploymentId()));
            try {
                String entityId = deployBrooklyn(deploymentContext, null, campYaml);
                deployedApps.put(brooklynNode.getId(), entityId);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        } else {
            boolean depMet = true;
            for (String dep : clGroup.getDependencies().get(brooklynNode.getId())) {

                Object depApp = deployedApps.get(dep);
                if(depApp == null){
                    //no cloudlightning service deployed
                    //try to see if a marathon app is deployed
                    String dep2 = "/" + group.getId() + "/" + dep.toLowerCase();
                    depApp = deployedApps.get(dep2);
                }
                if (depApp == null) {
                    //dependency is not currently deployed; add app back to queue
                    apps.add(brooklynNode);
                    depMet = false;
                    break;
                } else if (depApp instanceof App) {
                        App mApp = (App) depApp;
                        final List<Task> tasks = clGroup.getMarathonClients().get(dep.toLowerCase()).getAppTasks(mApp.getId()).getTasks();
                        boolean isUp = isServiceUp(tasks);

                        if(!isUp){
                            depMet = false;
                            apps.add(brooklynNode);
                            break;
                        }
                        Task task = tasks.get(0);
                        Map<String, String> brooklynConfig = addDependentPropsFromMarathon(brooklynNode,
                                mApp, deploymentContext, task);
                        if(brooklynConfig != null) {
                            brooklynConfig.put("tosca.deployment.id", deploymentContext.getDeploymentId());
                            campYaml.put("brooklyn.config", brooklynConfig);
                        }else{
                            campYaml.put("brooklyn.config", ImmutableMap.of("tosca.deployment.id", deploymentContext.getDeploymentId()));
                        }
                }else{
                    //TODO Retrieve details from brooklyn and add up Dependent endpoints
                    log.info("Not implemented yet!");
                }


            }
            if (depMet) {
//                List<Deployment> deployments = clGroup.getMarathonClients().get(app.getId()).getDeployments();
//                while (deployMentsContainDeps(deployments, app, group)) {
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    deployments = clGroup.getMarathonClients().get(app.getId()).getDeployments();
//                    log.info("Deployments: {}", deployments.size());
//                }

//                group = clGroup.getMarathonClients().get(app.getId()).getGroup(group.getId());
//                group.getApps().add(app);
//                group.setVersion(null);
//                clGroup.getMarathonClients().get(app.getId()).updateGroup(group);
//                campYaml.put("cloudlightning.config", ImmutableMap.of("tosca.deployment.id", deploymentContext.getDeploymentId()));
                try {
                    String entityId = deployBrooklyn(deploymentContext, null, campYaml);
                    deployedApps.put(brooklynNode.getId(), entityId);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        }
        return group;
    }

    private Map<String, Object> createBasicBrooklynService(PaaSTopologyDeploymentContext deploymentContext, CLGroup clGroup, PaaSNodeTemplate brooklynNode) {
        String topologyId = deploymentContext.getDeploymentTopology().getId();
        Map<String,Object> campYaml = Maps.newLinkedHashMap();
        addRootPropertiesAsCamp(deploymentContext, campYaml);

        List<Object> svcs = Lists.newArrayList();
        String nodeId = brooklynNode.getId();
        Map<String, Object> svc = Maps.newHashMap();
        svc.put("type", "alien4cloud_deployment_topology:" + topologyId + "$"+nodeId);
        svc.put("location", clGroup.getBrooklynLocation().get(nodeId));
//        Map<String, String> env = new HashMap();
////        env.put("RT_ENGINE", "$cloudlightning:config(\"raytrace_engine_ip_address\")");
////        svc.put("env", env);

//        svc.put("cloudlightning.parameters", props);
        svcs.add(svc);

        campYaml.put("services", svcs);
        return campYaml;
    }

    private boolean isServiceUp(List<Task> tasks) {
        boolean depMet = true;
        if (tasks.size() == 0) {
//                                    try {
//                                        Thread.sleep(1000);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
            depMet = false;
        } else {
            boolean breaking = false;
            for (Task t : tasks) {
                if (t.getHealthCheckResults() == null || t.getHealthCheckResults().stream().filter(c -> c.getAlive()).count() < t.getHealthCheckResults().size()) {
                    breaking = true;
                    break;
                }
            }
            if (breaking) {
                depMet = false;
            }
        }
        return depMet;
    }

    private String deployBrooklyn(PaaSTopologyDeploymentContext deploymentContext, IPaaSCallback<?> callback, Map<String, Object> campYaml) throws JsonProcessingException {
        try {
            useLocalContextClassLoader();
            String campYamlString = new ObjectMapper().writeValueAsString(campYaml);
            log.info("DEPLOYING: "+campYamlString);
            Response result = getNewBrooklynApi().getApplicationApi().createFromYaml( campYamlString );
            TaskSummary createAppSummary = BrooklynApi.getEntity(result, TaskSummary.class);
            log.info("RESULT: "+result.getEntity());
            validate(result);
            String entityId = createAppSummary.getEntityId();

            // inital entry which will immediately trigger an event in getEventsSince()
            return entityId;
        } catch (Throwable e) {
            log.warn("ERROR DEPLOYING", e);
//            knownDeployments.remove(deploymentContext.getDeploymentId());
            if (callback!=null) callback.onFailure(e);
            throw e;
        } finally { revertContextClassLoader(); }
    }

    private void validate(Response r) {
        if (r==null) return;
        if ((r.getStatus() / 100)==2) return;
        throw new IllegalStateException("Server returned "+r.getStatus() + " message " + r.getEntity());
    }

    private void addRootPropertiesAsCamp(PaaSTopologyDeploymentContext deploymentContext, Map<String,Object> result) {
        if (applicationService!=null) {
            try {
                Application app = applicationService.getOrFail(deploymentContext.getDeployment().getSourceId());
                if (app!=null) {
                    result.put("name", app.getName());
                    if (app.getDescription()!=null) result.put("description", app.getDescription());

                    List<String> tags = Lists.newArrayList();
                    for (Tag tag: app.getTags()) {
                        tags.add(tag.getName()+": "+tag.getValue());
                    }
                    if (!tags.isEmpty())
                        result.put("tags", tags);

                    // TODO icon, from app.getImageId());
                    return;
                }
                log.warn("Application null when deploying "+deploymentContext+"; using less information");
            } catch (NotFoundException e) {
                // ignore, fall through to below
                log.warn("Application instance not found when deploying "+deploymentContext+"; using less information");
            }
        } else {
            log.warn("Application service not available when deploying "+deploymentContext+"; using less information");
        }

        // no app or app service - use what limited information we have
        result.put("name", "A4C: "+deploymentContext.getDeployment().getSourceName());
        result.put("description", "Created by Alien4Cloud from application "+deploymentContext.getDeployment().getSourceId());
    }



    @Override
    public void undeploy(PaaSDeploymentContext deploymentContext, IPaaSCallback<?> callback) {
        log.info("UNDEPLOY " + deploymentContext + " / " + callback);
        CLGroup clGroup = cloudLightningDeployments.get(deploymentContext.getDeploymentId());

        if(clGroup != null) {
            for (String id : clGroup.getDeployedApps().keySet()) {
                Object o = clGroup.getDeployedApps().get(id);
                if (o instanceof App) {
                    App app = (App) o;
                    if (id.contains("/")) {
                        id = id.split("/")[2];
                    }
                    Result result = clGroup.getMarathonClients().get(id).deleteApp(app.getId());
                    clGroup.getMarathonEvents().get(id).registerDeployment(result.getDeploymentId(), deploymentContext.getDeploymentId(), DeploymentStatus.UNDEPLOYED);
                } else if (o instanceof String) {
                    //this is a Brooklyn entity id
                    String entityId = (String) o;
                    Response result = getNewBrooklynApi().getEntityApi().expunge(entityId, entityId, true);
                    validate(result);
                }
            }
            cloudLightningDeployments.remove(deploymentContext.getDeploymentId());
            this.deploymentStatuses.put(deploymentContext.getDeploymentId(), Optional.fromNullable(DeploymentStatus.UNDEPLOYED));
            knownDeployments.remove(deploymentContext.getDeploymentId());
            if (callback != null) {
                callback.onSuccess(null);
                this.deploymentStatuses.remove(deploymentContext.getDeploymentId());
            }
        }
    }

    @Override
    public void scale(PaaSDeploymentContext deploymentContext, String nodeTemplateId, int instances, IPaaSCallback<?> callback) {
        log.warn("SCALE not supported");
    }

    @Override
    public void getStatus(PaaSDeploymentContext deploymentContext, IPaaSCallback<DeploymentStatus> callback) {
        if (callback!=null) {
            String entityId = deploymentContext.getDeploymentId();
            log.info("GET STATUS - " + entityId);
            Optional<DeploymentStatus> deploymentStatus = deploymentStatuses.get(entityId);
            DeploymentStatus status = DeploymentStatus.UNDEPLOYED;
            if(cloudLightningDeployments.get(deploymentContext.getDeploymentId()) != null){
                status = DeploymentStatus.DEPLOYED;
            }
            callback.onSuccess(deploymentStatus.isPresent() ? deploymentStatus.get() : DeploymentStatus.UNDEPLOYED);
        }
    }

    @Override
    public void getInstancesInformation(PaaSTopologyDeploymentContext deploymentContext,
            IPaaSCallback<Map<String, Map<String, InstanceInformation>>> callback) {

        BrooklynApi brooklynApi = getNewBrooklynApi();
        CLGroup clGroup = cloudLightningDeployments.get(deploymentContext.getDeploymentId());
        Map<String, Map<String, InstanceInformation>> topology = Maps.newHashMap();
        for(String id : clGroup.getDeployedApps().keySet()) {
            // We lookup Entities based on tosca.id (getDeploymentId())
//            String appId = deploymentContext.getDeployment().getOrchestratorDeploymentId();
            Object o = clGroup.getDeployedApps().get(id);
            if(o instanceof String) {
                String appId = (String) o;
                List<EntitySummary> descendants = brooklynApi.getEntityApi().getDescendants(appId, appId, ".*");

                // TODO: Either get all sensors for all descendants, or iterate through the descendants,
                // building an InstanceInformation, and populating the topology

                for (EntitySummary descendant : descendants) {
                    String entityId = descendant.getId();
                    if (entityId.equals(appId)) {
                        continue;
                    }
                    String templateId = String.valueOf(brooklynApi.getEntityConfigApi().get(appId, entityId, "tosca.template.id", false));
                    // TODO: Work out what to do with clusters, for now assume it's all flat
                    InstanceInformation instanceInformation = new InstanceInformation();
                    Map<String, Object> sensorValues = brooklynApi.getSensorApi().batchSensorRead(appId, entityId, null);
                    ImmutableMap.Builder<String, String> sensorValuesStringBuilder = ImmutableMap.builder();
                    ImmutableMap.Builder<String, String> attributeValuesStringBuilder = ImmutableMap.builder();
                    for (Entry<String, Object> entry : sensorValues.entrySet()) {
                        if (entry.getKey().startsWith("tosca.attribute")) {
                            attributeValuesStringBuilder.put(entry.getKey(), String.valueOf(entry.getValue()));
                        } else {
                            sensorValuesStringBuilder.put(entry.getKey(), String.valueOf(entry.getValue()));
                        }
                    }
                    instanceInformation.setRuntimeProperties(sensorValuesStringBuilder.build());
                    instanceInformation.setAttributes(attributeValuesStringBuilder.build());
                    String serviceState = String.valueOf(sensorValues.get("service.state"));
                    instanceInformation.setState(serviceState);
                    instanceInformation.setInstanceStatus(LIFECYCLE_TO_INSTANCE_STATUS.get(Lifecycle.valueOf(serviceState)));
                    topology.put(templateId, ImmutableMap.of(entityId, instanceInformation));
                }
            }else if(o instanceof App){
                App app = (App) o;

                    try {
                        // Marathon tasks are alien instances
                        Map<String, InstanceInformation> instancesInfo = newHashMap();

                        PaaSNodeTemplate nodeTemplate2 = deploymentContext.getPaaSTopology().getAllNodes().get(id);
                        java.util.Optional<PaaSNodeTemplate> nodeTemplate = deploymentContext.getPaaSTopology().getNonNatives().stream()
                                .filter(p -> p.getId().toLowerCase().equals(app.getId().split("/")[2]))
                                .findFirst();
                        if(!nodeTemplate.isPresent()){
                            continue;
                        }
                        Map<String, IValue> attributes = nodeTemplate.get().getTemplate().getAttributes();
                        final Collection<Task> tasks = clGroup.getMarathonClients().get(nodeTemplate.get().getId().toLowerCase())
                                .getAppTasks(app.getId()).getTasks();
                        tasks.forEach(task -> {
                            final InstanceInformation instanceInformation = this.getInstanceInformation(task, attributes, deploymentContext.getPaaSTopology(), nodeTemplate.get());
                            instancesInfo.put(task.getId(), instanceInformation);
                        });

                        topology.put(nodeTemplate.get().getId(), instancesInfo);
                    } catch (MarathonException e) {
                        switch (e.getStatus()) {
                            case 404: // The app cannot be found in marathon - we display no information
                                break;
                            default:
                                callback.onFailure(e);
                        }
                    }
            }
        }

        callback.onSuccess(topology);
    }

    private InstanceInformation getInstanceInformation(Task task, Map<String, IValue> attributes, PaaSTopology paaSTopology, PaaSNodeTemplate paaSNodeTemplate) {
        InstanceInformation result = getInstanceInformation(task);
        for(String key : attributes.keySet()){
            String val = cloudLightningMappingService.retrieveValue(paaSNodeTemplate, paaSTopology, attributes.get(key), task);
            result.getAttributes().put(key, val);
        }
        return result;
    }

    /**
     * Get instance information, eg. status and runtime properties, from a Marathon Task.
     * @param task A Marathon Task
     * @return An InstanceInformation
     */
    private InstanceInformation getInstanceInformation(Task task) {
        final Map<String, String> runtimeProps = newHashMap();

        // Outputs Marathon endpoints as host:port1,port2, ...
        final Collection<String> ports = Collections2.transform(task.getPorts(), Functions.toStringFunction());
        runtimeProps.put("endpoint",
                "http://".concat(task.getHost().concat(":").concat(String.join(",", ports))));

        InstanceStatus instanceStatus;
        String state;

        // Leverage Mesos's TASK_STATUS - TODO: add Mesos 1.0 task states
        if(task.getState() != null) {
            switch (task.getState()) {
                case "TASK_RUNNING":
                    state = "started";
                    // Retrieve health checks results - if no healthcheck then assume healthy
                    instanceStatus =
                            java.util.Optional.ofNullable(task.getHealthCheckResults())
                                    .map(healthCheckResults ->
                                            healthCheckResults
                                                    .stream()
                                                    .findFirst()
                                                    .map(HealthCheckResults::getAlive)
                                                    .map(alive -> alive ? InstanceStatus.SUCCESS : InstanceStatus.FAILURE)
                                                    .orElse(InstanceStatus.PROCESSING)
                                    ).orElse(InstanceStatus.SUCCESS);
                    break;
                case "TASK_STARTING":
                    state = "starting";
                    instanceStatus = InstanceStatus.PROCESSING;
                    break;
                case "TASK_STAGING":
                    state = "creating";
                    instanceStatus = InstanceStatus.PROCESSING;
                    break;
                case "TASK_ERROR":
                    state = "stopped";
                    instanceStatus = InstanceStatus.FAILURE;
                    break;
                default:
                    state = "uninitialized"; // Unknown
                    instanceStatus = InstanceStatus.PROCESSING;
            }
        }else{
            state = "uninitialized";
            instanceStatus =
                    java.util.Optional.ofNullable(task.getHealthCheckResults())
                            .map(healthCheckResults ->
                                    healthCheckResults
                                            .stream()
                                            .findFirst()
                                            .map(HealthCheckResults::getAlive)
                                            .map(alive -> alive ? InstanceStatus.SUCCESS : InstanceStatus.FAILURE)
                                            .orElse(InstanceStatus.PROCESSING)
                            ).orElse(InstanceStatus.SUCCESS);
        }

        return new InstanceInformation(state, instanceStatus, newHashMap(), runtimeProps, newHashMap());
    }

    private void addTasksAndDescendants(Collection<TaskSummary> taskSummaries, Collection<AbstractMonitorEvent> events, String deploymentId,
            Date date, BrooklynApi brooklynApi) {
        for (TaskSummary taskSummary : taskSummaries) {
            Long startTime = taskSummary.getStartTimeUtc();
            if (Strings.isNonBlank(taskSummary.getDisplayName()) && startTime != null) {
                if (startTime > date.getTime()) {
                    // TODO: choose correct event type. See commented-out code below for event types
                    PaaSMessageMonitorEvent messageEvent = new PaaSMessageMonitorEvent();
                    messageEvent.setDate(taskSummary.getStartTimeUtc());
                    messageEvent.setDeploymentId(deploymentId);
                    messageEvent.setMessage(taskSummary.getEntityDisplayName() + " : " +  taskSummary.getDisplayName());
                    events.add(messageEvent);
                }
                Collection<TaskSummary> childSummaries;
                try {
                    childSummaries = brooklynApi.getActivityApi().children(taskSummary.getId(), false);
                    addTasksAndDescendants(childSummaries, events, deploymentId, date, brooklynApi);
                } catch (Exception ignored) {
                    // If we can't get the child tasks (usually because of a 404 if the task is transient), ignore
                    // the error and carry on
                }
            }
        }

//        try {
//            ApplicationSummary summary = getBrooklynApi().getApplicationApi().list("").iterator().next();
//            String applicationId = summary.getId();
//
//            List<EntitySummary> children = getBrooklynApi().getEntityApi().getChildren(applicationId, applicationId);
//
//            EntitySummary child = children.get(0);
//
//            String brooklynType = child.getType();
//            brooklynType = brooklynType.substring(brooklynType.lastIndexOf(".") + 1);
//            String deploymentId = String.valueOf(getBrooklynApi().getEntityConfigApi().get(applicationId, applicationId, "tosca.deployment.id", false));
//
//            // This is asking for events for the entire Brooklyn Management plane
//            // Get a list of tasks since ${date}, limit to maxEvents
//
//            PaaSDeploymentStatusMonitorEvent statusEvent = new PaaSDeploymentStatusMonitorEvent();
//            statusEvent.setDate(System.currentTimeMillis());  // Time of event!
//            statusEvent.setDeploymentId(deploymentId); // Need to look up the tosca.id which relates to this event
//            statusEvent.setDeploymentStatus(DeploymentStatus.DEPLOYED);
//            statusEvent.setDate(System.currentTimeMillis()); // Time of event!
//
//            PaaSInstanceStateMonitorEvent stateMonitorEvent = new PaaSInstanceStateMonitorEvent();
//            stateMonitorEvent.setAttributes(ImmutableMap.of("ip_address", "127.0.0.1")); // Find these in the alien console (tosca nomative types)
//            stateMonitorEvent.setRuntimeProperties(ImmutableMap.of("some.cloudlightning.sensor", "some.value"));
//            stateMonitorEvent.setInstanceStatus(InstanceStatus.SUCCESS);
//            stateMonitorEvent.setInstanceId(applicationId); // Broolkyn ID
//            stateMonitorEvent.setNodeTemplateId(brooklynType); // Brooklyn type
////        stateMonitorEvent.setCloudId("tosca.id"); // Internal only - Do not set
//            stateMonitorEvent.setDate(System.currentTimeMillis());  // Time of event!
//            stateMonitorEvent.setDeploymentId(deploymentId); // Need to look up the tosca.id which relates to this event
//
//            PaaSMessageMonitorEvent messageEvent = new PaaSMessageMonitorEvent();
//            messageEvent.setDate(System.currentTimeMillis());  // Time of event!
//            messageEvent.setDeploymentId(deploymentId); // Need to look up the tosca.id which relates to this event
//            messageEvent.setMessage("Hello from Brooklyn!");
//
//
//            // Deferred?
//            // ^^ set date, deploymentid etc
////        PaaSInstanceStorageMonitorEvent event4 = new PaaSInstanceStorageMonitorEvent();
////        event4.setDeletable(false); // These replace to block storage only
//
//            AbstractMonitorEvent[] events = {
//                    statusEvent,
//                    stateMonitorEvent,
//                    messageEvent
//            };
//            eventCallback.onSuccess(events);
//        } catch (Exception e) {
//            eventCallback.onFailure(e);
//        }

    }

    @Override
    public synchronized void getEventsSince(Date date, int maxEvents, IPaaSCallback<AbstractMonitorEvent[]> eventCallback) {
        BrooklynApi brooklynApi = getNewBrooklynApi();
        try {
            Collection<AbstractMonitorEvent> events = Sets.newHashSet();

            for(Entry<String, CLGroup> entry : cloudLightningDeployments.entrySet()){
                String deploymentId = entry.getKey();
                CLGroup clGroup = entry.getValue();
                for(EventService eventService : clGroup.getMarathonEvents().values()){
                    final List<AbstractMonitorEvent> eventList = eventService.getEventsSince(date, maxEvents);
                    events.addAll(eventList);
                }
                for(Object loc : clGroup.getDeployedApps().values()){
                    if(loc instanceof String){
                        ApplicationSummary appSummary = brooklynApi.getApplicationApi().get((String) loc);

                        if (appSummary != null) {
                            String appId = appSummary.getId();
//                            String deploymentId = String.valueOf(brooklynApi.getEntityConfigApi().get(appId, appId, "tosca.deployment.id", false));

                            for (EntitySummary entitySummary : brooklynApi.getEntityApi().getDescendants(appId, appId, null)) {
                                String entityId = entitySummary.getId();
                                List<TaskSummary> taskSummaries = brooklynApi.getEntityApi().listTasks(appId, entityId);
                                addTasksAndDescendants(taskSummaries, events, deploymentId, date, brooklynApi);
                            }
                            DeploymentStatus lastKnownStatus = deploymentStatuses.get(deploymentId).orNull();
                            DeploymentStatus currentStatus =  SERVICE_STATE_TO_DEPLOYMENT_STATUS.get(appSummary.getStatus());
                            if (!currentStatus.equals(lastKnownStatus)) {
                                PaaSDeploymentStatusMonitorEvent updatedStatus = new PaaSDeploymentStatusMonitorEvent();
                                updatedStatus.setDeploymentId(deploymentId);
                                updatedStatus.setDeploymentStatus(currentStatus);
                                events.add(updatedStatus);
                                deploymentStatuses.put(deploymentId, Optional.of(currentStatus));
                            }
                        }



                    }
                }
            }

            eventCallback.onSuccess(events.toArray(new AbstractMonitorEvent[]{}));
        } catch (Exception e) {
            eventCallback.onFailure(e);
        }
    }

    @Override
    public void executeOperation(PaaSTopologyDeploymentContext deploymentContext, NodeOperationExecRequest request,
            IPaaSCallback<Map<String, String>> operationResultCallback) throws OperationExecutionException {
        log.warn("EXEC OP not supported: " + request);
    }

    @Override
    public void switchMaintenanceMode(PaaSDeploymentContext deploymentContext, boolean maintenanceModeOn) throws MaintenanceModeException {
        log.info("MAINT MODE (ignored): " + maintenanceModeOn);
    }

    @Override
    public void switchInstanceMaintenanceMode(PaaSDeploymentContext deploymentContext, String nodeId, String instanceId, boolean maintenanceModeOn)
            throws MaintenanceModeException {
        log.info("MAINT MODE for INSTANCE (ignored): " + maintenanceModeOn);
    }

    @Override
    public void setConfiguration(Configuration configuration) throws PluginConfigurationException {
        log.info("Setting configuration: " + configuration);
        this.configuration = configuration;
    }

    protected BrooklynApi getNewBrooklynApi() {
        return BrooklynApi.newInstance(configuration.getUrl(), configuration.getUser(), configuration.getPassword());
    }
}
