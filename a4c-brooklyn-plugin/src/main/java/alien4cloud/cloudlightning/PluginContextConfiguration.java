package alien4cloud.cloudlightning;

import io.cloudsoft.tosca.metadata.BrooklynToscaTypeProvider;
import io.cloudsoft.tosca.metadata.DefaultToscaTypeProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Plugin spring configuration entry point.
 */
@Configuration
@ComponentScan(basePackages = {"alien4cloud.cloudlightning", "alien4cloud.cloudlightning.artifacts"})
public class PluginContextConfiguration {

    @Bean
    public DefaultToscaTypeProvider defaultToscaTypeProvider() {
        return new DefaultToscaTypeProvider();
    }

    @Bean
    public BrooklynToscaTypeProvider brooklynToscaTypeProvider() {
        return new BrooklynToscaTypeProvider();
    }
}